﻿using System;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

using Microsoft.AspNetCore.Http.Features;

using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace ImageCompressor.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImageController : Controller
    {
        [Route("{action}")]
        public void Compress()
        {
            var syncIOFeature = HttpContext.Features.Get<IHttpBodyControlFeature>();
            if (syncIOFeature != null)
            {
                syncIOFeature.AllowSynchronousIO = true;
            }

            if (Request.ContentLength > 0)
            {
                Console.WriteLine($"Request.ContentLength: {Request.ContentLength}");
                MemoryStream compressed = CompressImage(Request.Body);
                if (compressed.Length > 0)
                {
                    Console.WriteLine($"Compressed Length: {compressed.Length}");
                    Response.Body.Write(compressed.GetBuffer(), 0, (int)compressed.Length);
                }
            }
        }

        [Route("{action}")]
        public JsonResult Help()
        {
            return Json(new { StatusCode = Response.StatusCode, HelpUrl = "api/image/help", WorkUrl = "api/image/compress" });
        }

        private MemoryStream CompressImage(Stream data)
        {
            MemoryStream stream = new MemoryStream();
            try
            {
                using (var image = new Bitmap(data))
                {
                    Console.WriteLine("Bitmap created...");
                    ResizeImage(image, stream);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error: {e.Message}");
                return stream;
            }

            return stream;
        }

        private void ResizeImage(Bitmap image, Stream stream)
        {
            const int LONG_SIDE = 1500;
            const int DPI = 127;
            const long JPG_QUALITY = 50;

            Bitmap resized = image;

            int maxSide = Math.Max(image.Width, image.Height);

            if (maxSide >= LONG_SIDE)
            {
                float k = ((float)image.Width) / image.Height;
                if (k > 1)
                {
                    resized = new Bitmap(LONG_SIDE, (int)(LONG_SIDE / k));
                }
                else
                {
                    resized = new Bitmap((int)(LONG_SIDE * k), LONG_SIDE);
                }
                resized.SetResolution(DPI, DPI);

                using (var graphics = Graphics.FromImage(resized))
                {
                    Console.WriteLine("Graphics created...");

                    graphics.CompositingQuality = CompositingQuality.HighSpeed;
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.CompositingMode = CompositingMode.SourceCopy;
                    graphics.DrawImage(image, 0, 0, resized.Width, resized.Height);
                }

                var encoderParameters = new EncoderParameters(1);
                encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality, JPG_QUALITY);
                var jpgCodec = ImageCodecInfo.GetImageEncoders().FirstOrDefault(codec => codec.FormatID == ImageFormat.Jpeg.Guid);
                resized.Save(stream, jpgCodec, encoderParameters);
            }
        }
    }
}