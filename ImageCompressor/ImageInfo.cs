﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImageCompressor
{
    public class ImageInfo
    {
        //private byte[] data;

        //public ImageInfo() { }
        //public ImageInfo(long length)
        //{ 
            
        //}

        public bool IsResize { get; set; }
        public string Error { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int Dpi { get; set; }
        public string Base64Data { get; set; }
    }
}
